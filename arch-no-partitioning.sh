#!/bin/bash


# Updating keyring and  mirroslist_última
echo " #######  Installing keyring and reflector  #######"
sleep 2
clear
pacman -Sy archlinux-keyring --noconfirm 
clear
pacman -Sy reflector rsync --noconfirm 
sleep 3
clear
echo ""
echo "#######  Updating MirrorList  #######"
sleep 2
echo ""
reflector --verbose --latest 10 --sort rate --save /etc/pacman.d/mirrorlist
clear
cat /etc/pacman.d/mirrorlist
sleep 3
clear

# Installing base system
echo ""
echo "#######  Installing Base System  #######"
sleep 2
echo ""
pacstrap /mnt base base-devel nano reflector rsync git networkmanager
clear


echo ""
echo "Generating FSTAB"
sleep 2
genfstab -U /mnt >> /mnt/etc/fstab
cat /mnt/etc/fstab
sleep 4
clear


# Configuring  Pacman
echo "#######  Configuring Pacman.......  #######"
sleep 2
sed -i 's/#Color/Color/g' /mnt/etc/pacman.conf
sed -i 's/#TotalDownload/TotalDownload/g' /mnt/etc/pacman.conf
sed -i 's/#VerbosePkgLists/VerbosePkgLists/g' /mnt/etc/pacman.conf
sed -i "37i ILoveCandy" /mnt/etc/pacman.conf
sed -i '93d' /mnt/etc/pacman.conf
sed -i '94d' /mnt/etc/pacman.conf
sed -i "93i [multilib]" /mnt/etc/pacman.conf
sed -i "94i Include = /etc/pacman.d/mirrorlist" /mnt/etc/pacman.conf
clear


# Configuring hostname  and hosts
echo "#######  Choose Hostname  #######"
echo ""
echo " Choose Hostname"
read hostname
echo "$hostname" > /mnt/etc/hostname
echo "127.0.0.1 localhost" >> /mnt/etc/hosts
echo "::1 localhost" >> /mnt/etc/hosts
echo "127.0.1.1 $hostname.localdomain $hostname" >> /mnt/etc/hosts
echo "Hostname: $(cat /mnt/etc/hostname)"
echo ""
echo "Hosts: $(cat /mnt/etc/hosts)"
echo ""
clear


# Setting  User and Root
echo "#######  Users and Passwds  #######"
sleep 2
echo ""
echo "#######  ROOT Passwd  #######"
echo ""
echo " Choose root passwd "
read -s rootpasswd
sleep 2
echo ""
echo "#######   USER Name  #######"
echo ""
echo " Choose user name "
read user
sleep 2
echo ""
echo "#######   USER Passwd  #######"
echo ""
echo " Choose user passwd "
read -s userpasswd
sleep 2
echo ""
arch-chroot /mnt /bin/bash -c "(echo $rootpasswd ; echo $rootpasswd) | passwd root"
arch-chroot /mnt /bin/bash -c "useradd -m -g users -s /bin/bash $user"
arch-chroot /mnt /bin/bash -c "(echo $userpasswd ; echo $userpasswd) | passwd $user"
sed -i "80i $user ALL=(ALL) ALL"  /mnt/etc/sudoers
clear


# Configuring language and timezone

echo "#######  Updating TimeDate  #######"
sleep 2
echo "" 
echo -e ""
echo -e "\t\t\t| Actualizando Idioma del Sistema |"
printf '%*s\n' "${COLUMNS:-$(tput cols)}" '' | tr ' ' _
echo -e ""
echo "es_CO.UTF-8 UTF-8" > /mnt/etc/locale.gen
arch-chroot /mnt /bin/bash -c "locale-gen" 
echo "LANG=es_CO.UTF-8" > /mnt/etc/locale.conf
echo ""
cat /mnt/etc/locale.conf 
cat /mnt/etc/locale.gen
sleep 4
echo ""
arch-chroot /mnt /bin/bash -c "export $(cat /mnt/etc/locale.conf)" 
export $(cat /mnt/etc/locale.conf)
sleep 3
clear
arch-chroot /mnt /bin/bash -c "pacman -Sy curl --noconfirm"
arch-chroot /mnt /bin/bash -c "ln -sf /usr/share/zoneinfo/America/Bogota /etc/localtime"

echo "KEYMAP=latam" > /mnt/etc/vconsole.conf 
cat /mnt/etc/vconsole.conf 
sleep 2
clear
arch-chroot /mnt /bin/bash -c "timedatectl set-ntp true"
#arch-chroot /mnt /bin/bash -c "timedatectl set-timezone $(curl https://ipapi.co/timezone)"
#arch-chroot /mnt /bin/bash -c "pacman -S ntp --noconfirm"
#clear
#arch-chroot /mnt /bin/bash -c "ntpd -qg"
arch-chroot /mnt /bin/bash -c "hwclock --systohc"
sleep 3
clear

echo "#######  Updating MirrorList  #######"
sleep 2
echo ""
arch-chroot /mnt /bin/bash -c "reflector --verbose --latest 10 --sort rate --save /etc/pacman.d/mirrorlist"
clear
cat /mnt/etc/pacman.d/mirrorlist
sleep 3
clear

# Kernel installation

echo " Choose Kernel 1.Stable 2.Zen"
read kernel
if [ $kernel == 1 ]
then
	arch-chroot /mnt /bin/bash -c "pacman -S linux-firmware linux linux-headers dosfstools mtools mkinitcpio intel-ucode --noconfirm"
else
	arch-chroot /mnt /bin/bash -c "pacman -S linux-firmware linux-zen linux-zen-headers dosfstools mtools mkinitcpio intel-ucode --noconfirm"
fi

echo "#######  Installing and Configuring Grub  #######"
sleep 2

#Configuración para EFI
uefi=$( ls /sys/firmware/efi/ | grep -ic efivars )

if [ $uefi == 1 ]
then
	clear
	arch-chroot /mnt /bin/bash -c "pacman -S --needed grub efibootmgr --noconfirm"
	echo '' 
	echo 'Instalando EFI System >> bootx64.efi' 
	arch-chroot /mnt /bin/bash -c 'grub-install --target=x86_64-efi --efi-directory=/boot/efi  --removable' 
	echo '' 
	echo 'Instalando UEFI System >> grubx64.efi' 
	arch-chroot /mnt /bin/bash -c 'grub-install --target=x86_64-efi --efi-directory=/boot/efi --bootloader-id=VisoneArch'
	
	sed -i "6iGRUB_CMDLINE_LINUX_DEFAULT="loglevel=3"" /mnt/etc/default/grub
	sed -i '7d' /mnt/etc/default/grub
	
	echo ''
	arch-chroot /mnt /bin/bash -c "grub-mkconfig -o /boot/grub/grub.cfg"
	echo ''
	if [ $kernel == 1 ]
	then
	arch-chroot /mnt /bin/bash -c "mkinitcpio -p linux"
	else
	arch-chroot /mnt /bin/bash -c "mkinitcpio -p linux-zen"
	fi
	echo '' 
	echo 'ls -l /mnt/boot/efi' 
	ls -l /mnt/boot/efi 
	echo '' 
	sleep 4

else
	#configuración para MBR

# Chossing Disk to install
echo "print devices" | parted | grep /dev/ | awk '{if (NR!=1) {print}}'
echo ''
read -p "Introduce tu disco a instalar Arch: " disco

echo ''
echo "Selección de Disco: $disco"
sleep 3
echo ''

	clear
	arch-chroot /mnt /bin/bash -c "pacman -S --nedded grub --noconfirm"
	echo '' 
	arch-chroot /mnt /bin/bash -c "grub-install --target=i386-pc $disco"
	
	
	sed -i "6iGRUB_CMDLINE_LINUX_DEFAULT="loglevel=3"" /mnt/etc/default/grub
	sed -i '7d' /mnt/etc/default/grub
	
	echo ''
	arch-chroot /mnt /bin/bash -c "grub-mkconfig -o /boot/grub/grub.cfg"
	echo '' 
	echo 'ls -l /mnt/boot' 
	ls -l /mnt/boot 
	echo '' 
fi

# Machine options

echo "#######Choose System Mode --------------1.Laptop  2.Station  3.WM  #######"
read ch
if [ $ch == 1 ]
	then
		echo "#######  Installing xorg,gpu-drivers,terminal,wifi,DM,NM,BT and enabling services NM,BT,Fs  #######"
		arch-chroot /mnt /bin/bash -c "pacman -S --needed xorg xf86-input-libinput xf86-video-intel mesa lib32-mesa mesa-vdpau libva-mesa-driver lib32-mesa-vdpau lib32-libva-mesa-driver libva-vdpau-driver libvdpau-va-gl libva-utils vdpauinfo libvdpau lib32-libvdpau opencl-mesa clinfo ocl-icd lib32-ocl-icd opencl-headers xorg-xinit termite sddm iw wireless_tools wpa_supplicant dialog wireless-regdb bluez bluez-utils pulseaudio-bluetooth plasma-meta kde-applications-meta --noconfirm"
		arch-chroot /mnt /bin/bash -c " systemctl enable sddm bluetooth NetworkManager fstrim.timer"

elif [ $ch == 2 ]
	then
		echo "#######  Installing xorg,gpu-drivers,terminal,NM and enabling services NM,Fs  #######"
		arch-chroot /mnt /bin/bash -c "pacman -S --needed xorg xf86-input-libinput dkms nvidia-dkms nvidia-utils nvidia-settings xorg-xinit termite --noconfirm"
		arch-chroot /mnt /bin/bash -c " systemctl enable NetworkManager fstrim.timer"

elif [ $ch == 3 ]
	then
		echo "#######  Installing xorg,gpu-drivers,terminal,NM and enabling services NM  #######"
		arch-chroot /mnt /bin/bash -c "pacman -S --needed spice-vdagent xorg xf86-input-libinput xf86-video-fbdev spice-vdagent mesa mesa-libgl xorg-xinit termite spice-vdagent  --noconfirm"
		arch-chroot /mnt /bin/bash -c "systemctl enable NetworkManager"		
fi
sleep 2
clear

# Configuring Shell

echo "#######  Installing Zsh and Plugins and changing Shell  #######"
sleep 2
arch-chroot /mnt /bin/bash -c "pacman -S zsh zsh-completions zsh-syntax-highlighting zsh-autosuggestions zsh-history-substring-search --noconfirm"
SH=zsh
arch-chroot /mnt /bin/bash -c "chsh -s /bin/$SH"
arch-chroot /mnt /bin/bash -c "chsh -s /usr/bin/$SH $user"
arch-chroot /mnt /bin/bash -c "chsh -s /bin/$SH $user"
sleep 4
clear

# Installing Utilities

echo "#######  Installing Codecs,Xdg  #######"
sleep 2
#estás lineas son para instalar aplicaciones manuales, se comentan ya que con el DM instalado ya vienen instaladas.
# arch-chroot /mnt /bin/bash -c "pacman -S --needed ffmpeg gstreamer gst-plugins-good gst-libav gvfs gvfs-smb gvfs-mtp polkit-gnome gpart xdotool --noconfirm"
# arch-chroot /mnt /bin/bash -c "pacman -S --needed   wget neofetch lsb-release xdg-user-dirs --noconfirm"
arch-chroot /mnt /bin/bash -c "xdg-user-dirs-update"
clear


# Installing audio server and utilities

echo "#######  Installing Audio Server and Utilities  #######"
sleep 2
arch-chroot /mnt /bin/bash -c "pacman -S --needed pipewire-pulse gst-plugin-pipewire pavucontrol alsa-utils pamixer --noconfirm"
sleep 4
clear

# Installing fonts

echo "#######  Installing TTF  #######"
sleep 2
arch-chroot /mnt /bin/bash -c "pacman -S --needed bdf-unifont  ttf-nerd-fonts-symbols gnu-free-fonts cantarell-fonts ttf-hack ttf-inconsolata ttf-font-awesome ttf-liberation ttf-dejavu ttf-roboto noto-fonts ttf-bitstream-vera --noconfirm"
sleep 2
clear

# Installing applications

echo "#######  Installing Applications  #######"
sleep 2
# arch-chroot /mnt /bin/bash -c "pacman -S --needed pacman-contrib slop python-pywal usbutils neomutt ranger jq rofi firefox lxappearance feh mpv moc xclip telegram-desktop pcmanfm samba sxhkd kvantum-qt5 file-roller unzip --noconfirm"
arch-chroot /mnt /bin/bash -c "pacman -S --needed pacman-contrib usbutils firefox mpv xclip telegram-desktop samba kvantum-qt5 unzip --noconfirm"
sleep 2
clear

# Configurando makepkg.conf

echo "#######  Choose cores for compilation  ####### "
read core
sed -i 's/#MAKEFLAGS="J-2"/MAKEFLAGS="J-$core"/g' /mnt/etc/makepkg.conf
sed -i 's/COMPRESSXZ=(xz -c -z -)/COMPRESSXZ=(xz -c -z - --threads=0)/g' /mnt/etc/makepkg.conf
clear
sleep 2

# Installing AUR-Helper....

echo "#######  Installing yay...  #######"
sleep 2
echo "cd && git clone https://aur.archlinux.org/yay.git && cd yay && makepkg -si --noconfirm && cd && rm -rf yay-bin" | arch-chroot /mnt /bin/bash -c "su $user"
sleep 2
clear




# Unmounting partitins and reboot

echo "Do you want to umount partitions? y/n"
read part
if [ $part == y ]
	then
		umount -R /mnt
fi
clear

echo "#######  Do you want to reboot? y/n  #######"
read rb
if [ $rb == y ]
	then
		reboot
else
	exit
fi

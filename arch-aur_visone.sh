#!/bin/bash

# Installing AUR Applicatinos

echo "#######  Installing AUR Applicatinos  #######"
sleep 2
cd && sudo paru -Syu && paru -S picom-ibhagwan-git ttf-weather-icons polybar micro-bin autojump bspwm-rounded-corners-git bsp-layout brave-bin qmplay2 timeshift-bin colorz --noconfirm 
sleep 2
clear
echo " Do you want to install laptop applications  y/n"
read lap
if [ $lap == y ]
then

cd ~/.config && git clone https://github.com/dglt1/optimus-switch-amd.git && cd optimus-switch* && chmod +x * && sudo ./install.sh && sudo ./set-amd.sh

else

sleep 2
clear
echo "#######  Installing oh-my-zsh  #######"
sleep 2
cd && sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)" && source .zshrc
sleep 2
clear
fi
